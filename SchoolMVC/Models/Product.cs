﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SchoolMVC.Models
{
    public class Product
    {
        public int Id { get; set; }
        [Display(Name = "ProductName")]
        public string ProductName { get; set; }
        public decimal Price { get; set; }
        public string Description { get; set; }
        public virtual Category Category { get; set; }
        public string Photo { get; set; }


        [ForeignKey("Category")]
        public int Category_Id { get; set; }
    }
}