namespace SchoolMVC.Models
{
    using System;
    using System.Data.Entity;
    using System.Linq;

    public class SchoolContext : DbContext
    {
        public SchoolContext()
            : base("name=SchoolContext")
        {
            // Database.SetInitializer(new DropCreateDatabaseIfModelChanges<SchoolContext>());
        }

        public virtual DbSet<Product> Products { get; set; }
        public virtual DbSet<Category> Categories { get; set; }
                
        public DbSet<Cart> Carts { get; set; }       


    }
}
