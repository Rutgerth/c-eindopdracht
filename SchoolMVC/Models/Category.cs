﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SchoolMVC.Models
{
    public class Category
    {
        public int Id { get; set; }
        public string Type { get; set; }
        public virtual List<Product> Products { get; set; }
    }
}