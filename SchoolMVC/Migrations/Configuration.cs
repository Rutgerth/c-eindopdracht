namespace SchoolMVC.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using SchoolMVC.Models;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<SchoolMVC.Models.SchoolContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            ContextKey = "SchoolMVC.Models.SchoolContext";
        }
                    //private SchoolContext db = new SchoolContext();
        //Category = db.Categories.Single(s => s.Type == "Figures")
        protected override void Seed(SchoolMVC.Models.SchoolContext context)
        {
            context.Products.AddOrUpdate(m => m.ProductName,
                new Product { ProductName = "TF2 Engineer", Price = 55,
                    Description = "He kijk vriend, ik ben een engineer; dat betekent dat ik problemen oplos. Niet problemen zoals 'Wat is schoonheid?', want dat hangt af van het gezichtsveld van de woordraadsels van je eigen levensbeschouwing. Ik los praktische problemen op. � De Engineer over zijn beroep",
                    Photo = "TF2_Engineer.jpg", Category_Id = 1 },
                new Product { ProductName = "Pikachu", Price = 7,
                    Description = "Pikachu is een van de bekendste van de 649 Pok�mon uit de populaire gelijknamige anime-serie, alsook de Pok�moncomputer en -kaartspellen. Zijn nummer in de Pok�dex is #25. Hij ziet er uit als een gele hamster met konijnenoren, heeft rode wangetjes en een staart in de vorm van een bliksemschicht.",
                    Photo = "Pikachu.png", Category_Id = 1 },
                new Product { ProductName = "Pokemon Side bag", Price = 25, Description = "simpel Pok�mon - Schoudertas", Photo = "NewCollection.jpg", Category_Id = 2 },
                new Product { ProductName = "Yugiyo Booster pack", Price = 34,
                    Description = "Yu-Gi-Oh! is een Japanse strip of manga, die een groot mediafranchise heeft voortgebracht bestaande uit meerdere animatieseries, films, computerspellen en een ruilkaartspel",
                    Photo = "Yugiyo_Booster_pack.jpg", Category_Id = 3 },
                new Product { ProductName = "Cory in the House", Price = 99,
                    Description = "Cory in the House is een Amerikaanse komedieserie gecre�erd door Marc Warren en Dennis Rinsler en uitgezonden op Disney Channel. De serie is gemaakt voor kinderen en is een spin-off van de succesvolle televisieserie That's So Raven.",
                    Photo = "Cory_in_the_House.jpeg", Category_Id = 4 },
                new Product { ProductName = "Marvel Pack", Price = 60,
                    Description = "Het Marvel Universum is het fictieve gedeelde universum waarin de meeste stripverhalen van Marvel Comics zich afspelen. Het Marvel universum bestaat uit een groot multiversum van verschillende parallelle werelden en alternatieve tijdlijnen. De belangrijkste van deze tijdlijnen, waarin ook de meeste strips zich afspelen, staat bekend als Earth-616.",
                    Photo = "Marvel_Pack.jpg", Category_Id = 5 },
                new Product { ProductName = "Computer", Price = 800, Description = "Een simpelen computer setup.", Photo = "computer.jpg", Category_Id = 5 },


                new Product { ProductName = "Batman Action figure", Price = 65, Description = "Een simpelen computer setup.", Photo = "batman.jpg", Category_Id = 2 },
                new Product { ProductName = "Fortnite Action figure", Price = 15, Description = "Een simpelen computer setup.", Photo = "fortnite.jpg", Category_Id = 2 },
                new Product { ProductName = "Rick and Morty Action figure", Price = 20, Description = "Een simpelen computer setup.", Photo = "rick_and_morty.jpg", Category_Id = 2 },
                new Product { ProductName = "Blackpanther Action figure", Price = 25, Description = "Een simpelen computer setup.", Photo = "blackpanther.jpg", Category_Id = 2 },                
                new Product { ProductName = "Borderlands Action figure", Price = 40, Description = "Een simpelen computer setup.", Photo = "borderlands.jpg", Category_Id = 2 },

                new Product { ProductName = "SNES", Price = 100, Description = "Een simpelen computer setup.", Photo = "snes.jpg", Category_Id = 1 },
                new Product { ProductName = "Platenspeler", Price = 170, Description = "Een simpelen computer setup.", Photo = "platenspeler.jpg", Category_Id = 1 },
                new Product { ProductName = "Super nintendo", Price = 200, Description = "Een simpelen computer setup.", Photo = "super_nintendo.jpg", Category_Id = 1 },
                new Product { ProductName = "Gameboy", Price = 60, Description = "Een simpelen computer setup.", Photo = "gameboy.jpg", Category_Id = 1 },
                new Product { ProductName = "Gameboy color", Price = 90, Description = "Een simpelen computer setup.", Photo = "gameboy_color.jpg", Category_Id = 1 },

                new Product { ProductName = "Zelda bag", Price = 100, Description = "Een simpelen computer setup.", Photo = "zelda_bag.jpg", Category_Id = 3 },
                new Product { ProductName = "Nintendo bag", Price = 99, Description = "Een simpelen computer setup.", Photo = "nintendo_bag.jpg", Category_Id = 3 },
                new Product { ProductName = "Fallout bag", Price = 80, Description = "Een simpelen computer setup.", Photo = "fallout_bag.jpg", Category_Id = 3 },
                new Product { ProductName = "BUBM game bag", Price = 150, Description = "Een simpelen computer setup.", Photo = "bubm.jpg", Category_Id = 3 },
                new Product { ProductName = "Assassin's Creed bag", Price = 60, Description = "Een simpelen computer setup.", Photo = "Assassins_Creed.jpg", Category_Id = 3 },

                new Product { ProductName = "Fallout Trading cards", Price = 10, Description = "Een simpelen computer setup.", Photo = "Fallout_Trading_cards.jpg", Category_Id = 4 },
                new Product { ProductName = "Minecraft Trading cards", Price = 999, Description = "Een simpelen computer setup.", Photo = "Minecraft_Trading_cards.jpg", Category_Id = 4 },
                new Product { ProductName = "WWE Trading cards", Price = 5, Description = "Een simpelen computer setup.", Photo = "WWE_Trading_cards.jpg", Category_Id = 4 },
                new Product { ProductName = "Pokemon Trading cards", Price = 5, Description = "Een simpelen computer setup.", Photo = "Pokemon_Trading_cards.jpg", Category_Id = 4 },
                new Product { ProductName = "Star Wars Trading cards", Price = 9, Description = "Een simpelen computer setup.", Photo = "Starwars_Trading_cards.jpg", Category_Id = 4 },

                new Product { ProductName = "Dragon Ball Super", Price = 20, Description = "Een simpelen computer setup.", Photo = "Dragon_ball.jpg", Category_Id = 5 },
                new Product { ProductName = "Yu-Gi-Oh!", Price = 35, Description = "Een simpelen computer setup.", Photo = "Yu-Gi-Oh.jpg", Category_Id = 5 },
                new Product { ProductName = "Fullmetal Alchemist: Brotherhood", Price = 20, Description = "Een simpelen computer setup.", Photo = "Fullmetal_Alchemist.jpg", Category_Id = 5 },
                new Product { ProductName = "Hamtaro", Price = 10, Description = "Een simpelen computer setup.", Photo = "Hamtaro.jpg", Category_Id = 5 },
                new Product { ProductName = "Cowboy Bebop", Price = 25, Description = "Een simpelen computer setup.", Photo = "Cowboy_Bebop.jpg", Category_Id = 5 }


                );
            
            context.Categories.AddOrUpdate(m => m.Type,
                new Category { Type = "Figures" },
                new Category { Type = "Bags" },
                new Category { Type = "Trading Cards" },
                new Category { Type = "Manga" },
                new Category { Type = "Other Merchandise" }
                );
        }
    }
}
